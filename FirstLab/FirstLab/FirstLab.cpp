#include <iostream>
#include <cmath>
#include <cassert>


using namespace std;

unsigned int sumRealPart(float number) {
		number = abs(number);
	unsigned int sum = 0;
	for (int i = 10; i <= 1000; i *= 10) {
		sum += (static_cast<unsigned int>(number*i)) % 10;
	}
	return sum;
}


int main()
{
	assert(sumRealPart(1.1137f) == 5);
	assert(sumRealPart(1.3333333f) == 9);
	assert(sumRealPart(1.00f) == 0);
	assert(sumRealPart(1.102f) == 3);
	assert(sumRealPart(-2.206f) == 8);

	float number;
	cout << "Enter a real number: " << endl;
	cin>>number;
	unsigned int result = sumRealPart(abs(number));
	cout<<"Result: "<<result<<endl;
	

	return 0;
}